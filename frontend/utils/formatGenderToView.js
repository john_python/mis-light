const GENDER = {
  MALE: 'Мужчина',
  FEMALE: 'Женщина',
  UNKNOW: 'Человек'
};

const formatGenderToView = gender => {
  if (gender === 'Male') return GENDER.MALE;

  if (gender === 'Female') return GENDER.FEMALE;

  return GENDER.UNKNOW;
};

export default formatGenderToView;
