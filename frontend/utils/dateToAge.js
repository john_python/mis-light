const formatYearsToAge = years => {
  if (years < 9 || years > 15) {
    if (years % 10 === 1) return `${years} год`;
    if (years % 10 < 5) return `${years} года`;
  }

  return `${years} лет`;
};

const formatMonthsToAge = months => {
  if (months % 10 === 1) return `${months} месяц`;
  if (months % 10 < 5) return `${months} месяца`;

  return `${months} месяцев`;
};

const formatDaysToAge = days => {
  if (days < 9 || days > 15) {
    if (days % 10 === 1) return `${days} день`;
    if (days && days % 10 < 5) return `${days} дня`;
  }

  return `${days} дней`;
};

const dateToAge = date => {
  if (!date) return "";
  const birthDate = new Date(date).toLocaleDateString();
  const currentDate = new Date().toLocaleDateString();

  const [birthDay, birthMonth, birthYear] = birthDate.split(".");
  const [currentDay, currentMonth, currentYear] = currentDate.split(".");

  let age = currentYear - birthYear + (birthMonth < currentMonth) - 1;

  if (currentMonth === birthMonth) {
    age += currentDay >= birthDay;
  }

  if (age) return formatYearsToAge(age);

  age = currentMonth - birthMonth + (birthDay < currentDay) - 1;

  if (age) return formatMonthsToAge(age);

  if (birthMonth < currentMonth) {
    const daysInMonth = 32 - new Date(birthYear, birthMonth, 32).getDate();
    return formatDaysToAge(daysInMonth - birthDay + currentDay);
  }

  return formatDaysToAge(currentDay - birthDay);
};

export default dateToAge;
