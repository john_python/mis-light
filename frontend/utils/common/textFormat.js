export const sliceText = (description = "", maxLength = 50) =>
  description.length > maxLength
    ? description.slice(0, maxLength) + "..."
    : description;