import DateFnsUtils from '@date-io/date-fns';
import { SERVER_FORMAT, DEFAULT_FORMAT } from 'constants/date';

const dateFns = new DateFnsUtils();

export const formatDateToClient = (date, formatString = DEFAULT_FORMAT) => {
  if (dateFns.isValid(new Date(date))) {
    return dateFns.format(new Date(date), formatString);
  }

  return dateFns.format(new Date(), formatString);
};

export const formatDateToServer = date => dateFns.format(date, SERVER_FORMAT);
