import dateToAge from "./dateToAge";
import initialsForAvatar from "./initialsForAvatar";
import formatGenderToView from "./formatGenderToView";
import formatNameCaseBlockToView from "./formatNameCaseBlockToView";

export {
  dateToAge,
  initialsForAvatar,
  formatGenderToView,
  formatNameCaseBlockToView
};
