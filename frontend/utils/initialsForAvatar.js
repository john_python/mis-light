const initialsForAvatar = fullName =>
  fullName
    .split(' ')
    .slice(0, 2)
    .map(string => string[0]);

export default initialsForAvatar;
