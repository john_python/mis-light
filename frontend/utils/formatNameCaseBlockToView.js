const formatNameCaseBlockToView = type => {
  if (type === 'complaints') {
    return 'Жалобы';
  }

  if (type === 'examination') {
    return 'Осмотр';
  }

  if (type === 'medications') {
    return 'Медикаменты';
  }

  return undefined;
};

export default formatNameCaseBlockToView;
