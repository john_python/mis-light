import { useState } from 'react';

const openModal = setIsOpen => () => {
  setIsOpen(true);
};

const closeModal = setIsOpen => () => {
  setIsOpen(false);
};

function useModal(initialIsOpen = false) {
  const [isOpen, setIsOpen] = useState(initialIsOpen);

  const actions = {
    open: openModal(setIsOpen),
    close: closeModal(setIsOpen)
  };

  return [isOpen, actions];
}

export default useModal;
