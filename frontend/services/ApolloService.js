class ApolloService {
  constructor() {
    this.apollo = {};
  }

  init(apolloClient) {
    this.apollo = apolloClient;
  }
}

export default new ApolloService();
