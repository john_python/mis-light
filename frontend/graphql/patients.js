import { gql } from "apollo-boost";

// NEW QUERIES
const PATIENTS = gql`
  query getPatients($start: Int, $limit: Int, $where: JSON) {
    patients(start: $start, limit: $limit, where: $where) {
      id
      name
      comment
      phone
      birthDate
    }
  }
`;

const PATIENT_BY_ID = gql`
  query getPatient($id: ID!) {
    patient(id: $id) {
      id
      name
      comment
      phone
      birthDate
    }
  }
`;

const CREATE_PATIENT = gql`
  mutation createPatient($input: createPatientInput) {
    createPatient(input: $input) {
      patient {
        id
      }
    }
  }
`;

export { CREATE_PATIENT, PATIENTS, PATIENT_BY_ID };
