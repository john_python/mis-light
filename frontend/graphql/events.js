import { gql } from "apollo-boost";

const EVENTS_BY_PATIENT_ID = gql`
  query getVisitByPatientId(
    $start: Int
    $limit: Int
    $sort: String
    $where: JSON
  ) {
    events(start: $start, limit: $limit, sort: $sort, where: $where) {
      id
      comment
      startDate
      created_at
    }
  }
`;

const EVENTS_BY_STARTDATE = gql`
  query getEventsByStartDate($sort: String, $where: JSON) {
    events(sort: $sort, where: $where) {
      id
      comment
      startDate
      created_at
      patient {
        id
        name
        birthDate
      }
    }
  }
`;

const EVENTS = gql`
  query getAllEvents($sort: String) {
    events(sort: $sort) {
      id
      comment
      startDate
      created_at
      patient {
        name
      }
    }
  }
`;

const CREATE_EVENT = gql`
  mutation createEvent($input: createEventInput) {
    createEvent(input: $input) {
      event {
        id
      }
    }
  }
`;

const UPDATE_EVENT = gql`
  mutation updateEvent($input: updateEventInput) {
    updateEvent(input: $input) {
      event {
        id
      }
    }
  }
`;

const DELETE_EVENT = gql`
  mutation deleteEvent($input: deleteEventInput) {
    deleteEvent(input: $input) {
      event {
        id
      }
    }
  }
`;

export {
  EVENTS_BY_PATIENT_ID,
  CREATE_EVENT,
  UPDATE_EVENT,
  DELETE_EVENT,
  EVENTS,
  EVENTS_BY_STARTDATE
};
