import AnalyzesIcon from "./AnalyzesIcon.svg";
import CalendarIcon from "./CalendarIcon.svg";
import ListIcon from "./ListIcon.svg";
import ArrowIcon from "./ArrowIcon.svg";
import ClockIcon from "./ClockIcon.svg";
import PrescriptionIcon from "./PrescriptionIcon.svg";
import AlcoIcon from "./AlcoIcon.svg";
import HealthGroupIcon from "./HealthGroupIcon.svg";
import CigaretteIcon from "./CigaretteIcon.svg";
import DispensaryIcon from "./DispensaryIcon.svg";
import QueueIcon from "./QueueIcon.svg";
import DonorIcon from "./DonorIcon.svg";
import DrugIcon from "./DrugIcon.svg";
import GeoIcon from "./GeoIcon.svg";
import PhoneIcon from "./PhoneIcon.svg";
import CodeIcon from "./CodeIcon.svg";
import OperationIcon from "./OperationIcon.svg";
import MessageIcon from "./MessageIcon.svg";
import SyringeIcon from "./SyringeIcon.svg";
import HospitalIcon from "./HospitalIcon.svg";
import DoctorsAvatar from "./DoctorsAvatar.svg";
import LogoIcon from "./LogoIcon.svg";
import HomeIcon from "./HomeIcon.svg";
import ScheduleIcon from "./ScheduleIcon.svg";
import UserIcon from "./UserIcon.svg";
import ReportIcon from "./ReportIcon.svg";
import ChronicIcon from "./ChronicIcon.svg";
import DiseaseIcon from "./DiseaseIcon.svg";
import InfoIcon from "./InfoIcon.svg";
import SettingsIcon from "./SettingsIcon.svg";
import SearchIcon from "./SearchIcon.svg";
import FilterIcon from "./FilterIcon.svg";
import PlusIcon from "./PlusIcon.svg";
import PillsIcon from "./PillsIcon.svg";
import TrashIcon from "./TrashIcon.svg";
import EditIcon from "./EditIcon.svg";
import DateIcon from "./DateIcon.svg";
import LungsIcon from "./LungsIcon.svg";
import HeadacheIcon from "./HeadacheIcon.svg";
import PopoverEllipsisIcon from "./PopoverEllipsisIcon.svg";
import InputArrowIcon from "./inputArrow.svg";
import CheckedIcon from "./CheckedIcon.svg";
import ManUser from "./manUser.svg";
import WomanUser from "./womanUser.svg";
import ForwardIcon from "./ForwardIcon.svg";
import NotFoundIcon from "./NotFoundIcon.svg";
import TimeIcon from "./TimeIcon.svg";
import ChangeViewIcon from "./ChangeViewIcon.svg";

export {
  AnalyzesIcon,
  CalendarIcon,
  ClockIcon,
  HealthGroupIcon,
  ListIcon,
  ArrowIcon,
  PrescriptionIcon,
  DoctorsAvatar,
  DispensaryIcon,
  HospitalIcon,
  AlcoIcon,
  CigaretteIcon,
  DonorIcon,
  DrugIcon,
  QueueIcon,
  GeoIcon,
  PhoneIcon,
  CodeIcon,
  OperationIcon,
  MessageIcon,
  SyringeIcon,
  LogoIcon,
  HomeIcon,
  ScheduleIcon,
  UserIcon,
  ReportIcon,
  ChronicIcon,
  DiseaseIcon,
  InfoIcon,
  SettingsIcon,
  SearchIcon,
  FilterIcon,
  PlusIcon,
  PillsIcon,
  TrashIcon,
  EditIcon,
  DateIcon,
  LungsIcon,
  HeadacheIcon,
  PopoverEllipsisIcon,
  InputArrowIcon,
  CheckedIcon,
  ManUser,
  WomanUser,
  ForwardIcon,
  NotFoundIcon,
  TimeIcon,
  ChangeViewIcon
};
