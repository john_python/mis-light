import React from "react";
import { TextField } from "@material-ui/core";

export default ({ input, meta, ...rest }) => (
  <TextField
    {...input}
    {...rest}
    errorText={meta.touched ? meta.error : ""}
  />
);
