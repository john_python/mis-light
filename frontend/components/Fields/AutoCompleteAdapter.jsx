import React, { useState } from "react";
import Downshift from "downshift";
import { TextField, Paper, MenuItem } from "@material-ui/core";
import { deburr } from "lodash";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    height: 250
  },
  container: {
    flexGrow: 1,
    position: "relative"
  },
  paper: {
    position: "absolute",
    zIndex: 1,
    marginTop: theme.spacing(1),
    left: 0,
    right: 0
  },
  chip: {
    margin: theme.spacing(0.5, 0.25)
  },
  inputRoot: {
    flexWrap: "wrap"
  },
  inputInput: {
    width: "auto",
    flexGrow: 1
  },
  divider: {
    height: theme.spacing(2)
  }
}));

function renderSuggestion(suggestionProps) {
  const {
    suggestion,
    index,
    itemProps,
    highlightedIndex,
    viewPath
  } = suggestionProps;
  const isHighlighted = highlightedIndex === index;

  return (
    <MenuItem
      {...itemProps}
      key={suggestion[viewPath]}
      selected={isHighlighted}
      component="div"
      onClick={e => {
        console.log("suggestion", suggestion);
        itemProps.onClick(suggestion);
        itemProps.onChange(suggestion);
      }}
    >
      {suggestion[viewPath]}
    </MenuItem>
  );
}

function getSuggestions(data, value, viewPath, { showEmpty = false } = {}) {
  const inputValue = deburr(value.trim()).toLowerCase();
  const inputLength = inputValue.length;
  let count = 0;

  console.log("DATA", data);

  return inputLength === 0 && !showEmpty
    ? []
    : data.filter(suggestion => {
        const keep =
          count < 5 &&
          suggestion[viewPath].slice(0, inputLength).toLowerCase() ===
            inputValue;

        if (keep) {
          count += 1;
        }

        return keep;
      });
}

function renderInput(inputProps) {
  const { InputProps, classes, ref, ...other } = inputProps;

  return (
    <TextField
      required
      InputProps={{
        inputRef: ref,
        classes: {
          root: classes.inputRoot,
          input: classes.inputInput
        },
        ...InputProps
      }}
      {...other}
    />
  );
}

export default ({ input, meta, label, data, viewPath, ...rest }) => {
  const classes = useStyles();

  return (
    <Downshift {...input} {...meta} {...rest} onChange={() => {}}>
      {({
        clearSelection,
        getInputProps,
        getItemProps,
        getLabelProps,
        getMenuProps,
        highlightedIndex,
        inputValue,
        isOpen,
        openMenu,
        selectedItem
      }) => {
        const { onBlur, onChange, onFocus, ...inputProps } = getInputProps({
          onChange: event => {
            if (event.target.value === "") {
              clearSelection();
            }
          },
          onFocus: openMenu
        });

        return (
          <div className={classes.container}>
            {renderInput({
              fullWidth: true,
              classes,
              label,
              InputLabelProps: getLabelProps({ shrink: true }),
              InputProps: { onBlur, onChange, onFocus },
              inputProps
            })}

            <div {...getMenuProps()}>
              {isOpen ? (
                <Paper className={classes.paper} square>
                  {getSuggestions(data, inputValue, viewPath, {
                    showEmpty: true
                  }).map((suggestion, index) =>
                    renderSuggestion({
                      suggestion,
                      viewPath,
                      index,
                      itemProps: getItemProps({
                        item: suggestion.name,
                        onChange: input.onChange
                      }),
                      highlightedIndex,
                      selectedItem
                    })
                  )}
                </Paper>
              ) : null}
            </div>
          </div>
        );
      }}
    </Downshift>
  );
};
