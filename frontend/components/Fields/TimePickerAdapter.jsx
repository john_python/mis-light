import React from "react";
import { KeyboardTimePicker } from "@material-ui/pickers";

export default ({ input, meta, ...rest }) => (
  <KeyboardTimePicker {...input} {...rest} ampm={false} />
);
