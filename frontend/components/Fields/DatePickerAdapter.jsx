import React from "react";
import { KeyboardDatePicker } from "@material-ui/pickers";

export default ({ input, meta, ...rest }) => (
  <KeyboardDatePicker {...input} {...rest} format="dd/MM/yyyy" />
);
