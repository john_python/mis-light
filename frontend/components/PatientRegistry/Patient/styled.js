import styled from "styled-components";

import { Avatar, LinearProgress, ListItem } from "@material-ui/core";

const Item = styled.span`
  display: flex;
  align-items: center;
  color: ${({ theme }) => theme.palette.gray.main};
  font-size: 1rem;
`;

const Profile = styled(ListItem)`
  display: flex;
  align-items: stretch;
  justify-content: space-between;
  margin-bottom: 12px;
  padding: 12px 24px;
  box-sizing: border-box;
  border-radius: 6px;
  background: ${({ theme }) => theme.palette.background.secondary};
  color: ${({ theme }) => theme.palette.secondary.light};
  border-left: 10px solid ${p => p.theme.palette.primary.light};
  cursor: pointer;
  transition: all 0.2s;
  box-shadow: ${({ theme }) => theme.options.card.boxShadow};

  &:hover {
    background: ${({ theme }) => theme.palette.background.primary};
    box-shadow: ${({ theme }) => theme.options.card.boxShadowHover};
  }
`;

const AvatarStyled = styled(Avatar)`
  width: 48px;
  height: 48px;
  margin-right: 24px;
  background: ${({ theme }) => theme.palette.secondary.main};
  color: ${({ theme }) => theme.palette.primary.main};
`;

const LastVisit = styled(Item)`
  width: 160px;

  svg {
    margin-right: 5px;
    color: ${({ theme }) => theme.palette.primary.main};
  }
`;

const LinearProgressStyled = styled(LinearProgress)`
  flex: 1;
  max-width: 300px;
  padding: 12px;
  margin: 8px 12px;
  box-sizing: border-box;
  background-color: ${({ theme }) => theme.palette.gray.light};
  & > div {
    background-image: linear-gradient(
      to right,
      ${({ theme }) => theme.palette.gray.light} 0%,
      white 45%,
      white 65%,
      ${({ theme }) => theme.palette.gray.light} 100%
    );
  }
`;

///////////////

const PatientCol = styled.div`
  flex: 1 0 25%;
  display: flex;
  align-items: center;
  justify-content: ${p => p.justifyContent || "center"};
  padding-right: 20px;

  &:not(:last-of-type) {
    border-right: 2px solid ${p => p.theme.palette.divider};
  }
`;

const AddInfo = styled.span`
  color: ${({ theme }) => theme.palette.gray.main};
`;

const FullName = styled.span`
  color: ${({ theme }) => theme.palette.text.primary};
  font-size: 1.1rem;
`;

const NameAge = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
`;

export {
  Profile,
  AvatarStyled,
  FullName,
  AddInfo,
  NameAge,
  LinearProgressStyled,
  PatientCol,
  LastVisit
};
