import React from "react";
import { useRouter } from "next/router";
import PropTypes from "prop-types";
import { IconButton, Tooltip } from "@material-ui/core";

import { ManUser, WomanUser, ForwardIcon, DateIcon } from "static/icons";
import { dateToAge } from "utils";

import {
  Profile,
  AvatarStyled,
  FullName,
  AddInfo,
  LastVisit,
  LinearProgressStyled,
  PatientCol,
  NameAge
} from "./styled";

const Patient = ({ loading, data }) => {
  const router = useRouter();

  if (loading) {
    return (
      <Profile>
        <PatientCol justifyContent="flex-start">
          <AvatarStyled>
            <ManUser />
          </AvatarStyled>
          <NameAge>
            <FullName>
              <LinearProgressStyled />
            </FullName>
            <AddInfo>
              <LinearProgressStyled />
            </AddInfo>
          </NameAge>
        </PatientCol>
        <PatientCol>
          <LastVisit>
            <DateIcon />
            <LinearProgressStyled />
          </LastVisit>
        </PatientCol>
        <IconButton>
          <ForwardIcon />
        </IconButton>
      </Profile>
    );
  }

  const { id, name, comment, phone, birthDate } = data;
  const redirectToPatient = () => router.push(`/patients/${id}`);

  return (
    <Profile button onClick={redirectToPatient}>
      <PatientCol justifyContent="flex-start">
        <AvatarStyled>
          <ManUser />
        </AvatarStyled>
        <NameAge>
          <FullName>{name}</FullName>
          <AddInfo>{[dateToAge(birthDate), phone].join(", ")}</AddInfo>
        </NameAge>
      </PatientCol>
      <PatientCol>
        <Tooltip title="Последний визит">
          <LastVisit>
            <DateIcon />
            15.11.2019
          </LastVisit>
        </Tooltip>
      </PatientCol>
      <IconButton>
        <ForwardIcon />
      </IconButton>
    </Profile>
  );
};

Patient.propTypes = {
  loading: PropTypes.bool,
  data: PropTypes.object
};

export default Patient;
