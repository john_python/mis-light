import React from "react";
import { Form, Field } from "react-final-form";
import styled from "styled-components";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import { useMutation } from "react-apollo-hooks";
import { Button, Grid } from "@material-ui/core";
import { TextFieldAdapter } from "components/Fields";
import { CREATE_PATIENT } from "graphQL/patients";
import FnsUtils from "@date-io/date-fns";
import ruLocale from "date-fns/locale/ru";
import { DatePickerAdapter } from "components/Fields";

const AddPatient = ({ query, variables, onClose }) => {
  const [addPatient, { loading }] = useMutation(CREATE_PATIENT);
  const onSubmit = values => {
    addPatient({
      variables: {
        input: {
          data: values
        }
      },
      update() {
        onClose();
      },
      refetchQueries: [{ query, variables }]
    });
  };

  return (
    <Wrapper>
      <Form
        onSubmit={onSubmit}
        initialValues={{
          birthDate: new Date()
        }}
        render={({ handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <MuiPickersUtilsProvider utils={FnsUtils} locale={ruLocale}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Field
                    fullWidth
                    required
                    name="name"
                    label="Имя"
                    component={TextFieldAdapter}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Field
                    required
                    fullWidth
                    label="Дата рождения"
                    name="birthDate"
                    component={DatePickerAdapter}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Field
                    fullWidth
                    name="phone"
                    label="Телефон"
                    component={TextFieldAdapter}
                  />
                </Grid>

                <Grid item xs={12}>
                  <Field
                    fullWidth
                    name="comment"
                    label="Комментарий"
                    component={TextFieldAdapter}
                  />
                </Grid>
              </Grid>
              <ButtonWrapper>
                <Button
                  disabled={loading}
                  color="primary"
                  variant="contained"
                  type="submit"
                >
                  Добавить
                </Button>
              </ButtonWrapper>
            </MuiPickersUtilsProvider>
          </form>
        )}
      />
    </Wrapper>
  );
};

export default AddPatient;

const Wrapper = styled.div`
  padding: 20px;
  min-width: 600px;
`;

const ButtonWrapper = styled.div`
  margin-top: 20px;
  text-align: right;
`;
