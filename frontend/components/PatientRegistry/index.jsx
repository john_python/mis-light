import React from "react";

import { PATIENTS } from "graphQL/patients";
import ControllableList from "components/common/controls/ControllableList";
import Patient from "./Patient";
import AddPatient from "./AddPatient";

const PatientRegistry = () => {
  // TODO: просто для наглядности, если будут variables
  const variables = {};

  return (
    <>
      <ControllableList
        title="Пациенты"
        listPath="patients"
        actionTitle="Добавить пациента"
        query={PATIENTS}
        Component={Patient}
        variables={variables}
        searchField="name"
        DialogComponent={AddPatient}
        dialogTitle="Добавление пациента"
      />
    </>
  );
};
export default PatientRegistry;
