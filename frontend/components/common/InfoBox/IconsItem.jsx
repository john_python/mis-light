import React from 'react';
import PropTypes from 'prop-types';

import {
  AnalyzesIcon,
  PrescriptionIcon,
  OperationIcon,
  MessageIcon
} from 'static/icons';

const Icon = ({ type }) => {
  if (type === 'analysis') return <AnalyzesIcon />;
  if (type === 'prescription') return <PrescriptionIcon />;
  if (type === 'operation') return <OperationIcon />;
  if (type === 'initialVisit') return <MessageIcon />;
};

Icon.propTypes = {
  type: PropTypes.string.isRequired
};

export default Icon;
