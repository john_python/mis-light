import React from 'react';
import PropTypes from 'prop-types';

import ExpansionPanel from "../ExpansionPanel";

import { ArrowIcon } from 'static/icons';

import ShortItem from './ShortItem';

import Info, {
  ExpansionPanelDetailsStyled,
  ExpansionPanelSummaryStyled
} from './styled';

const InfoBox = ({
  isExpanded,
  defaultExpanded,
  title,
  className,
  data,
  children,
  toggleExpanded
}) => (
  <Info className={className}>
    <ExpansionPanel
      defaultExpanded={defaultExpanded}
      expanded={isExpanded}
      onChange={toggleExpanded}
    >
      <ExpansionPanelSummaryStyled expandIcon={<ArrowIcon />}>
        <Info.Title>{title}</Info.Title>
      </ExpansionPanelSummaryStyled>
      <ExpansionPanelDetailsStyled>
        <Info.Content>
          {children || (
            <Info.List>
              {data.map(({ guid, type, title: itemTitle, date }) => (
                <ShortItem
                  key={guid}
                  guid={guid}
                  type={type}
                  title={itemTitle}
                  date={date}
                />
              ))}
            </Info.List>
          )}
        </Info.Content>
      </ExpansionPanelDetailsStyled>
    </ExpansionPanel>
  </Info>
);

InfoBox.propTypes = {
  isExpanded: PropTypes.bool,
  defaultExpanded: PropTypes.bool,
  title: PropTypes.any.isRequired,
  className: PropTypes.string,
  data: PropTypes.array,
  children: PropTypes.any,
  toggleExpanded: PropTypes.func
};

export default InfoBox;
