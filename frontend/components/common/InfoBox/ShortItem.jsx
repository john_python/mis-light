import React from 'react';
import PropTypes from 'prop-types';

import { formatDateToClient } from 'utils/common/formatDate';

import Icon from './IconsItem';

import Info from './styled';

const handleClick = guid => console.log(`click to ${guid}`);

const ShortItem = ({ guid, type, title, date }) => (
  <Info.ItemSmall onClick={() => handleClick(guid)}>
    <Info.Icon>
      <Icon type={type} />
    </Info.Icon>
    <Info.Content>
      <Info.Inner>{title}</Info.Inner>
    </Info.Content>
    <Info.Date>{formatDateToClient(date, 'dd.MM.yy')}</Info.Date>
  </Info.ItemSmall>
);

ShortItem.propTypes = {
  date: PropTypes.string,
  type: PropTypes.string,
  title: PropTypes.string,
  guid: PropTypes.string.isRequired
};

export default ShortItem;
