import React from 'react';
import PropTypes from 'prop-types';

import { formatDateToClient } from 'utils/common/formatDate';

import Icon from './IconsItem';

import Info from './styled';

const titleByType = type => {
  if (type === 'analysis') return 'Анализ';
  if (type === 'prescription') return 'Назначение';
};

const handleClick = guid => console.log(`click to ${guid}`);

const FullItem = ({ guid, type, title, date }) => (
  <Info.Item onClick={() => handleClick(guid)}>
    <Info.Icon>
      <Icon type={type} />
    </Info.Icon>
    <Info.Content>
      <Info.Title>{titleByType(type)}</Info.Title>
      <Info.Inner>{title}</Info.Inner>
    </Info.Content>
    <Info.Date>{formatDateToClient(date, 'dd.MM.yy')}</Info.Date>
  </Info.Item>
);

FullItem.propTypes = {
  date: PropTypes.string,
  type: PropTypes.string,
  title: PropTypes.string,
  guid: PropTypes.string.isRequired
};

export default FullItem;
