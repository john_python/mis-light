import styled from "styled-components";

import {
  ExpansionPanelDetails,
  ExpansionPanelSummary
} from "@material-ui/core";

const Info = styled.div`
  margin-bottom: 12px;

  & > div {
    box-shadow: unset;
  }
`;

Info.Head = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 16px;
`;

Info.Title = styled.h5`
  margin: 0;
  font-size: 1.4rem;
  color: ${({ theme }) => theme.palette.text.primary};
`;

Info.More = styled.span`
  font-size: 1.2rem;
  color: ${({ theme }) => theme.palette.primary.main};
  cursor: pointer;

  &:hover {
    color: ${({ theme }) => theme.palette.primary.light};
  }
`;

Info.List = styled.ul`
  display: block;
  box-sizing: border-box;
  padding: 0;
  padding-bottom: 10px;
  margin: 0;
`;

Info.Item = styled.li`
  display: flex;
  align-items: center;
  margin: 0 0 10px;
  cursor: pointer;
`;

Info.ItemSmall = styled.li`
  display: flex;
  align-items: center;
  padding: 10px 14px;
  cursor: pointer;

  &:hover {
    background: ${({ theme }) => theme.palette.gray.light};
  }
`;

Info.Icon = styled.div`
  margin-right: 10px;
  display: flex;

  svg {
    width: 24px;
    height: 24px;
  }
`;

Info.IconSmall = styled.div`
  display: flex;
  align-items: center;
  margin-right: 10px;

  svg {
    width: 16px;
    height: 16px;
  }
`;

Info.Content = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  justify-content: space-between;
  font-size: 1.4rem;
  color: ${({ theme }) => theme.palette.gray.dark};

  a {
    color: ${({ theme }) => theme.palette.gray.dark};
    text-decoration: underline;
    cursor: pointer;

    &:hover {
      text-decoration: none;
    }
  }
`;

Info.Inner = styled.p`
  padding: 0;
  margin: 0;
  font-size: 1.4rem;
`;

Info.Date = styled.span`
  margin-left: auto;
  font-size: 1.4rem;
  color: ${({ theme }) => theme.palette.gray.dark};
`;

Info.DateSmall = styled.span`
  margin-left: auto;
  font-size: 1.2rem;
  color: ${({ theme }) => theme.palette.gray.dark};
`;

const ExpansionPanelDetailsStyled = styled(ExpansionPanelDetails)`
  padding: unset;
`;

const ExpansionPanelSummaryStyled = styled(ExpansionPanelSummary)`
  min-height: 48px !important;
  max-height: 48px !important;
  padding-left: 12px;
  padding-right: 12px;
`;

export {
  Info as default,
  ExpansionPanelDetailsStyled,
  ExpansionPanelSummaryStyled
};
