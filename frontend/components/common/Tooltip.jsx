import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const TooltipContent = styled.div`
  display: none;
  position: absolute;
  top: calc(100% + 8px);
  right: -12px;
  padding: 6px 8px;
  background: ${({ theme }) => theme.palette.background.secondary};
  border-radius: 8px;
  box-shadow: ${({ theme }) => theme.options.card.boxShadow};

  &:after {
    content: '';
    display: block;
    width: 16px;
    height: 16px;
    position: absolute;
    top: -8px;
    right: 20px;
    background: ${({ theme }) => theme.palette.background.secondary};
    transform: rotate(45deg);
  }
`;

const TooltipContainer = styled.div`
  position: relative;

  &:hover ${TooltipContent} {
    display: block;
  }
`;

const Tooltip = ({ content, children }) => (
  <TooltipContainer>
    {children}
    <TooltipContent>{content}</TooltipContent>
  </TooltipContainer>
);

Tooltip.propTypes = {
  content: PropTypes.node.isRequired,
  children: PropTypes.node.isRequired
};

export default Tooltip;
