import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Router from 'next/router';

import { Button } from '@material-ui/core';
import { PlusIcon } from 'static/icons';

const ButtonStyled = styled(Button)`
  background: ${({ theme }) => theme.palette.secondary.main};
  color: ${({ theme }) => theme.palette.secondary.contrastText};
  font-weight: bold;

  &:hover {
    background: ${({ theme }) => theme.palette.secondary.dark};
  }

  svg {
    fill: ${({ theme }) => theme.palette.secondary.contrastText};
    margin-right: 10px;
  }
`;

const LinkButton = ({ children, query, path }) => (
  <ButtonStyled onClick={() => Router.push({ pathname: path, query })}>
    <PlusIcon />
    {children}
  </ButtonStyled>
);

LinkButton.propTypes = {
  children: PropTypes.string,
  query: PropTypes.object,
  path: PropTypes.string.isRequired
};

export default LinkButton;
