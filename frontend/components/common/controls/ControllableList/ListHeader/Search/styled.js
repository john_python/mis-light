import styled from "styled-components";

import { IconButton, TextField } from "@material-ui/core";

const HeaderSearchStyled = styled(TextField)`
  flex-grow: 0;
  margin: 0 24px 0 0;
  background: ${({ theme }) => theme.palette.secondary.dark};
  color: ${({ theme }) => theme.palette.secondary.contrastText};
  border-radius: 20px;

  input {
    padding: 8px 20px 8px 10px;
    width: 200px;
    transition: width 0.2s;

    &:focus {
      width: 350px;
      transition: width 0.3s;
    }
  }

  svg {
    z-index: 1;
  }
`;

const IconButtonStyled = styled(IconButton)`
  margin-right: 10px;

  svg {
    fill: ${({ theme }) => theme.palette.primary.contrastText};
  }

  &:hover {
    background: ${({ theme }) => theme.palette.secondary.light};

    svg {
      fill: ${({ theme }) => theme.palette.primary.main};
    }
  }
`;

export { HeaderSearchStyled, IconButtonStyled };
