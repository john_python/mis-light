import { debounce } from "lodash-es";
import styled from "styled-components";
import React, { useState, useCallback, useEffect } from "react";
import PropTypes from "prop-types";
import { InputAdornment } from "@material-ui/core";

import { SearchIcon } from "static/icons";

import { HeaderSearchStyled } from "./styled";

const Search = ({ setSearch, search }) => {
  const [inputValue, setInputValue] = useState(search);

  useEffect(() => {
    if (search === "") setInputValue("");
  }, [search]);

  const onSearch = useCallback(debounce(value => setSearch(value), 1000), []);

  const onInputChange = event => {
    const { value } = event.target;

    setInputValue(value);
    onSearch(value);
  };

  return (
    <HeaderSearchStyled
      value={inputValue}
      onChange={onInputChange}
      InputProps={{
        disableUnderline: true,
        startAdornment: (
          <SearchIconWrapper>
            <SearchIcon />
          </SearchIconWrapper>
        )
      }}
    />
  );
};

Search.propTypes = {
  setSearchValue: PropTypes.func
};

export default Search;

const SearchIconWrapper = styled(InputAdornment)`
  margin-left: 15px;
  color: ${({ theme }) => theme.palette.primary.contrastText};
`;
