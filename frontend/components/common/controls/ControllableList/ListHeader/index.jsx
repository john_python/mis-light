import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Button } from "@material-ui/core";
import { PlusIcon } from "static/icons";

import { Header, Title, HeaderButtons } from "./styled";
import Search from "./Search";

const ListHeader = ({
  className,
  title,
  actionTitle,
  openModal,
  linkQuery,
  setSearch,
  search
}) => {
  return (
    <Header className={className}>
      <Title>{title}</Title>
      <HeaderButtons>
        <Search search={search} setSearch={setSearch} />
        {openModal && (
          <Action variant="outlined" onClick={openModal}>
            <PlusIcon />
            {actionTitle}
          </Action>
        )}
      </HeaderButtons>
    </Header>
  );
};

ListHeader.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  actionTitle: PropTypes.string,
  linkQuery: PropTypes.object,
  openModal: PropTypes.func,
  setSearch: PropTypes.func,
  search: PropTypes.string
};

export default ListHeader;

const Action = styled(Button)`
  color: ${p => p.theme.palette.primary.contrastText};
  border-color: currentColor;
  border-radius: 20px;

  svg {
    margin-right: 20px;
  }
`;
