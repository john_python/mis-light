import styled from "styled-components";

import { IconButton } from "@material-ui/core";

const Header = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 12px;
  padding: 24px;
  padding-bottom: 70px;
  border-radius: 0 0 20px 20px;
  height: 60px;
  background: ${p => p.theme.palette.primary.main};
  color: ${p => p.theme.palette.primary.contrastText};
`;

const Title = styled.h3`
  font-size: 18px;
  font-weight: bold;
  margin: 0;
`;

const IconButtonStyled = styled(IconButton)`
  margin-right: 10px;
  color: ${({ theme, active }) =>
    active ? theme.palette.primary.dark : theme.palette.gray.main};

  &:hover {
    background: ${({ theme }) => theme.palette.secondary.main};
    color: ${({ theme }) => theme.palette.secondary.contrastText};
  }
`;

const HeaderButtons = styled.div`
  display: flex;
  align-items: center;
`;

export { Header, Title, IconButtonStyled, HeaderButtons };
