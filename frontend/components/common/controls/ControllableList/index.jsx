import React, { useState } from "react";
import PropTypes from "prop-types";
import { Dialog, DialogTitle } from "@material-ui/core";

import ListHeader from "./ListHeader";
import List from "./List";
import styled from "styled-components";
import { useModal } from "utils/hooks";

const ControllableList = ({
  className,
  title,
  listPath,
  actionTitle,
  query,
  variables,
  Component,
  searchField,
  DialogComponent,
  dialogComponentProps,
  dialogTitle
}) => {
  const [search, setSearch] = useState();
  const [open, modalActions] = useModal();

  const queryVariables = {
    ...variables,
    where: {
      ...variables.where,
      [searchField + "_contains"]: search
    }
  };

  return (
    <Wrapper>
      <ListHeader
        className={className}
        title={title}
        actionTitle={actionTitle}
        openModal={modalActions.open}
        setSearch={setSearch}
        search={search}
      />
      <ListWrapper>
        <List
          listPath={listPath}
          search={search}
          query={query}
          variables={queryVariables}
          Component={Component}
          resetSearch={setSearch.bind(null, "")}
        />
      </ListWrapper>
      <Dialog open={open} onClose={modalActions.close}>
        <DialogTitle>{dialogTitle}</DialogTitle>
        <DialogComponent
          onClose={modalActions.close}
          query={query}
          variables={queryVariables}
          {...dialogComponentProps}
        />
      </Dialog>
    </Wrapper>
  );
};

ControllableList.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  actionTitle: PropTypes.string,
  query: PropTypes.object,
  variables: PropTypes.object,
  Component: PropTypes.func,
  DialogComponent: PropTypes.func,
  dialogComponentProps: PropTypes.object,
  dialogTitle: PropTypes.string
};

export default ControllableList;

const Wrapper = styled.div``;

const ListWrapper = styled.div`
  padding: 24px;
  margin-top: -64px;
`;
