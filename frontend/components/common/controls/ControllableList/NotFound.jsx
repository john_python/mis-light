import React from "react";
import styled from "styled-components";
import { Button } from "@material-ui/core";
import { NotFoundIcon } from "static/icons";

const NotFound = ({ search, reset }) => (
  <Wrapper>
    <NotFoundIcon />
    <p>
      По запросу <b>{search}</b> ничего не найдено
    </p>
    <Button onClick={reset} color="primary" variant="outlined">
      Сбросить поиск
    </Button>
  </Wrapper>
);

export default NotFound;

const Wrapper = styled.div`
  margin-top: 20vh;

  p {
    font-size: 1.5rem;
    margin: 20px 0;
  }
`;
