import React from "react";
import PropTypes from "prop-types";
import { useQuery } from "react-apollo-hooks";
import NotFound from "./NotFound";

const List = ({
  query,
  variables,
  search,
  Component,
  listPath,
  resetSearch
}) => {
  const { data, error, loading } = useQuery(query, {
    variables,
    fetchPolicy: "cache-and-network"
  });

  if (loading)
    return Array(10)
      .fill(0)
      .map((_, i) => <Component key={i.toString()} loading />);

  if (error) return <span>{error.toString()}</span>;

  return (
    <>
      {search && !data[listPath]?.length && (
        <div align="center">
          <NotFound search={search} reset={resetSearch} />
        </div>
      )}
      {data[listPath].map(item => (
        <Component
          key={item.id}
          data={item}
          query={query}
          variables={variables}
        />
      ))}
      {!search && !data[listPath]?.length && (
        <h3 align="center">Список пуст</h3>
      )}
    </>
  );
};

List.propTypes = {
  query: PropTypes.object,
  Component: PropTypes.func
};

export default List;
