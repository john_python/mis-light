import React from 'react';
import PropTypes from 'prop-types';

import {
  AlcoIcon,
  CigaretteIcon,
  DonorIcon,
  DrugIcon,
  ChronicIcon,
  DiseaseIcon,
  HealthGroupIcon
} from 'static/icons';

import { TagList, ChipStyled } from './styled';

const icons = {
  alcoholAddiction: <AlcoIcon />,
  drugAddiction: <DrugIcon />,
  smoking: <CigaretteIcon />,
  chronicDiseases: <ChronicIcon />,
  disability: <DonorIcon />,
  dispensaryObservation: <DonorIcon />,
  pregnancy: <DonorIcon />,
  industrialHazardType: <DonorIcon />,
  palliativeStatus: <DonorIcon />,
  allergens: <DonorIcon />,
  intolerableMedications: <DonorIcon />,
  isHeredityBurden: <DonorIcon />,
  disease: <DiseaseIcon />,
  healthGroup: <HealthGroupIcon />
};

const isEmptyChildren = children => !children;

const Tags = ({ children, className, style }) => {
  return (
    <TagList className={className} style={style}>
      {children}
    </TagList>
  );
};

const Tag = ({ iconType, children, className, ...otherProps }) => {
  if (iconType) {
    return (
      <ChipStyled
        icon={icons[iconType]}
        label={children}
        size="small"
        className={className}
        isEmpty={isEmptyChildren(children)}
        {...otherProps}
      />
    );
  }

  return (
    <ChipStyled
      label={children}
      size="small"
      className={className}
      isEmpty={isEmptyChildren(children)}
      {...otherProps}
    />
  );
};

Tags.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
  className: PropTypes.string,
  style: PropTypes.object
};

Tag.propTypes = {
  iconType: PropTypes.string,
  children: PropTypes.string
};

Tag.defaultProps = {
  type: 'secondary'
};

export { Tags, Tag };
