import styled from 'styled-components';

import { Chip } from '@material-ui/core';

const TagList = styled.ul`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -4px;
  padding: 0;
`;

const ChipStyled = styled(Chip)`
  margin: 4px;
  padding: 0 4px;
  background: ${({ theme, type }) => theme.palette[type].main};
  color: ${({ theme, type }) => theme.palette[type].contrastText};
  font-size: 0.8rem;

  &:hover {
    background: ${({ theme, type }) => theme.palette[type].dark};
  }

  &:active {
    background: ${({ theme, type }) => theme.palette[type].light};
  }

  svg {
    margin: 0;
    fill: ${({ theme, type }) => theme.palette[type].contrastText};
  }

  & > span {
    display: block;
    max-width: 160px;
    white-space: nowrap;
    overflow: hidden;
    padding: ${({ isEmpty }) => (isEmpty ? '0' : '')};
    padding-top: 0.2rem;
    padding-bottom: 0.2rem;
    text-overflow: ellipsis;
  }
`;

export { TagList, ChipStyled };
