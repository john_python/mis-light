import React, { useState, useCallback } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import {
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  ExpansionPanel as ExpansionPanelKit
} from "@material-ui/core";

import { ArrowIcon } from "static/icons";

const ExpansionPanelStyled = styled(ExpansionPanelKit)`
  margin: 0 !important;
  box-shadow: unset;
`;

const ExpansionPanelSummaryStyled = styled(ExpansionPanelSummary)`
  min-height: 48px !important;
  display: flex;
  justify-content: space-between;
  width: 100%;

  .expand-button {
    flex-basis: 104px !important;
    flex-shrink: 0 !important;
    background: ${({ theme }) => theme.palette.secondary.main};
    border-radius: 4px;
    color: ${({ theme }) => theme.palette.primary.main};
    font-size: 1.4rem;
    padding: 8px 10px;
    text-align: center;
  }

  & > div {
    margin: 0 !important;
  }

  & > div:nth-child(1) {
    overflow: hidden;
  }

  & > div:nth-child(2) {
    flex-basis: 40px;
  }
`;

const ExpansionPanelDetailsStyled = styled(ExpansionPanelDetails)`
  padding: 0;
`;

const ExpandButton = ({ isOpen }) => (
  <span>{isOpen ? "Скрыть" : "Показать"}</span>
);

const buttonProps = {
  style: {
    transform: "none"
  },
  classes: {
    root: "expand-button"
  }
};

const ExpansionPanel = ({
  title,
  children,
  className,
  classes,
  buttonExpand,
  defaultExpanded
}) => {
  // Не рендерим содержимое children в DOM в целях оптимизации страницы
  // в случае, если панель свернута
  const [expanded, setExpanded] = useState(defaultExpanded || false);
  const toggleExpanded = useCallback(() => setExpanded(!expanded), [expanded]);

  const expandIcon = buttonExpand ? (
    <ExpandButton isOpen={expanded} />
  ) : (
    <ArrowIcon />
  );

  return (
    <ExpansionPanelStyled
      className={className}
      classes={classes}
      defaultExpanded={defaultExpanded}
    >
      <ExpansionPanelSummaryStyled
        expandIcon={expandIcon}
        IconButtonProps={buttonExpand && buttonProps}
        onClick={toggleExpanded}
      >
        {title}
      </ExpansionPanelSummaryStyled>
      {expanded && (
        <ExpansionPanelDetailsStyled>{children}</ExpansionPanelDetailsStyled>
      )}
    </ExpansionPanelStyled>
  );
};

ExpandButton.propTypes = {
  isOpen: PropTypes.bool
};

ExpansionPanel.propTypes = {
  buttonExpand: PropTypes.bool,
  defaultExpanded: PropTypes.bool,
  className: PropTypes.string,
  classes: PropTypes.object,
  title: PropTypes.node.isRequired,
  children: PropTypes.node.isRequired
};

export default ExpansionPanel;
