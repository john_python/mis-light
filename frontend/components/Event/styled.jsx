import styled from "styled-components";

import { LinearProgress, Avatar, IconButton } from "@material-ui/core";
import { CalendarIcon, HospitalIcon } from "static/icons/index";

const CalendarIconStyled = styled(CalendarIcon)`
  fill: ${({ theme }) => theme.palette.gray.main};
`;

const HospitalIconStyled = styled(HospitalIcon)`
  fill: ${({ theme }) => theme.palette.primary.main};

  & > circle {
    fill: ${({ theme }) => theme.palette.secondary.light};
  }
`;

const EventContainer = styled.div`
  border-left: 10px solid
    ${p => (p.past > -1 ? p.theme.palette.primary.light : "#b9c1ca")};
  margin-bottom: 12px;
  padding: 14px 24px;
  box-shadow: ${({ theme }) => theme.options.card.boxShadow};
  background: ${({ theme }) => theme.palette.background.secondary};
  border-radius: 4px;
  transition: all 0.2s;
  display: flex;
  justify-content: space-between;
  align-items: center;

  &:hover {
    box-shadow: ${({ theme }) => theme.options.card.boxShadow};
  }
`;

const EventInfo = styled.div`
  overflow: hidden;
`;

const EventTitle = styled.h3`
  margin: 0;
  margin-top: 5px;
  color: ${({ theme }) => theme.palette.text.primary};
  font-size: 1rem;
  font-weight: 400;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const EventTitleWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const EventStatus = styled.span`
  margin-left: 12px;
  color: ${({ theme }) => theme.palette.primary.main};
  font-size: 0.8rem;
  font-weight: 500;
  white-space: nowrap;
`;

const EventDate = styled.div`
  display: flex;
  width: 200px;
  color: ${({ theme }) => theme.palette.gray.main};
  font-size: 0.9rem;
`;

const DateIconWrapper = styled.div`
  margin-right: 6px;
`;

const Organization = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: auto;
  margin-right: 16px;
  border-left: 1px solid ${({ theme }) => theme.palette.divider};
`;

const OrganizationInfo = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 16px;
`;

const OrganizationName = styled.span`
  font-size: 0.9rem;
  color: ${({ theme }) => theme.palette.text.primary};
`;

const OrganizationDetails = styled.span`
  text-align: right;
  font-size: 0.9rem;
  color: ${({ theme }) => theme.palette.gray.main};
`;

const OrganizationIconWrapper = styled.div`
  margin-left: 8px;
`;

const Doctor = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  border-left: 1px solid ${({ theme }) => theme.palette.divider};
`;

const DoctorInfo = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 16px;
`;

const DoctorName = styled.span`
  font-size: 0.9rem;
  color: ${({ theme }) => theme.palette.text.primary};
`;

const DoctorPosition = styled.span`
  text-align: right;
  font-size: 0.9rem;
  color: ${({ theme }) => theme.palette.gray.main};
`;

const DoctorIconWrapper = styled.div`
  margin-left: 8px;
`;

const AvatarStyled = styled(Avatar)`
  width: 40px;
  height: 40px;
  background: ${({ theme }) => theme.palette.secondary.main};
  color: ${({ theme }) => theme.palette.primary.main};
`;

const LinearProgressStyled = styled(LinearProgress)`
  flex: 1;
  min-width: 100px;
  padding: 6px;
  margin: 4px;
  box-sizing: border-box;
  background-color: ${({ theme }) => theme.palette.gray.light};

  & > div {
    background-image: linear-gradient(
      to right,
      ${({ theme }) => theme.palette.gray.light} 0%,
      white 45%,
      white 65%,
      ${({ theme }) => theme.palette.gray.light} 100%
    );
  }
`;

const Actions = styled.div`
  display: flex;
  align-items: center;
`;

const Action = styled(IconButton)`
  width: 46px;
  height: 46px;
  display: flex;
  align-items: center;
  justify-content: center;

  color: ${p => p.theme.palette.gray.main};
  opacity: 0.7;

  &:hover {
    opacity: 1;
    color: ${p => p.theme.palette.error.main};
  }
`;

export {
  CalendarIconStyled,
  HospitalIconStyled,
  EventContainer,
  EventInfo,
  EventTitle,
  EventTitleWrapper,
  EventStatus,
  EventDate,
  DateIconWrapper,
  Organization,
  OrganizationInfo,
  OrganizationName,
  OrganizationDetails,
  OrganizationIconWrapper,
  Doctor,
  DoctorInfo,
  DoctorName,
  DoctorPosition,
  DoctorIconWrapper,
  LinearProgressStyled,
  AvatarStyled,
  Actions,
  Action
};
