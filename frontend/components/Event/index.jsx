import React from "react";
import PropTypes from "prop-types";
import { formatDateToClient } from "utils/common/formatDate";
import { compareAsc } from "date-fns";
import { TrashIcon } from "static/icons";
import { Tooltip } from "@material-ui/core";
import { useMutation, useQuery } from "react-apollo-hooks";

import Loading from "./Loading";

import {
  CalendarIconStyled,
  EventContainer,
  EventInfo,
  EventTitle,
  EventDate,
  DateIconWrapper,
  EventTitleWrapper,
  Action,
  Actions
} from "./styled";
import { DELETE_EVENT } from "graphQL/events";

const Event = ({ loading, data, className, query, variables }) => {
  if (loading) {
    return <Loading />;
  }

  const { id, comment, startDate } = data;
  const [deleteEvent, { loading: deleteIsProcessing }] = useMutation(
    DELETE_EVENT,
    {
      variables: {
        input: {
          where: {
            id
          }
        }
      },
      onCompleted: function() {
        console.log("completed");
      },
      onError: function() {
        console.log("error");
      },
      refetchQueries: [{ query, variables }]
    }
  );

  return (
    <EventContainer
      className={className}
      past={compareAsc(new Date(startDate), new Date())}
    >
      <div>
        <EventInfo>
          <EventDate>
            <DateIconWrapper>
              <CalendarIconStyled />
            </DateIconWrapper>
            {formatDateToClient(startDate, "dd.MM.yyyy HH:mm")}
          </EventDate>
        </EventInfo>
        <EventTitleWrapper>
          <EventTitle>{comment}</EventTitle>
        </EventTitleWrapper>
      </div>
      <Actions>
        <Tooltip title="Удалить">
          <Action disabled={deleteIsProcessing} onClick={deleteEvent}>
            <TrashIcon />
          </Action>
        </Tooltip>
      </Actions>
    </EventContainer>
  );
};

Event.propTypes = {
  loading: PropTypes.bool,
  className: PropTypes.string,
  onClick: PropTypes.func,
  data: PropTypes.object,
  query: PropTypes.string,
  variables: PropTypes.object
};

export default Event;
