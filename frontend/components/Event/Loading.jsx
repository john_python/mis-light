import React from "react";
import PropTypes from "prop-types";

import {
  CalendarIconStyled,
  EventContainer,
  EventInfo,
  EventDate,
  DateIconWrapper,
  LinearProgressStyled,
  EventTitle,
  EventTitleWrapper
} from "./styled";

const Loading = ({ className }) => (
  <EventContainer className={className}>
    <EventInfo>
      <EventDate>
        <DateIconWrapper>
          <CalendarIconStyled />
        </DateIconWrapper>
        <LinearProgressStyled />
      </EventDate>
    </EventInfo>
    <EventTitleWrapper>
      <EventTitle>
        <LinearProgressStyled />
      </EventTitle>
    </EventTitleWrapper>
  </EventContainer>
);

Loading.propTypes = {
  className: PropTypes.string
};

export default Loading;
