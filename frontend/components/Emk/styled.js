import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  align-items: flex-start;
  overflow-y: auto;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  height: 100%;
  overflow-y: auto;
`;

export { Container, Content };
