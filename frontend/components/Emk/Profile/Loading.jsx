import React from "react";
import styled from "styled-components";

import {
  PatientProfile,
  AvatarStyled,
  ProfileInfo,
  FullName,
  Details,
  Detail,
  ProfileComment,
  LinearProgressStyled
} from "./styled";

const Loading = () => (
  <PatientProfile>
    <AvatarStyled />
    <ProfileInfo>
      <FullName>
        <LinearProgress />
      </FullName>
      <Details>
        <Detail>
          <LinearProgress />
        </Detail>
      </Details>
    </ProfileInfo>
    <ProfileComment>
      <LinearProgress />
    </ProfileComment>
  </PatientProfile>
);

export default Loading;

const LinearProgress = styled(LinearProgressStyled)`
  opacity: 0.2;
`;
