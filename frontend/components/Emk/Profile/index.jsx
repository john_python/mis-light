import React from "react";

import { dateToAge, initialsForAvatar } from "utils";

import Loading from "./Loading";

import {
  PatientProfile,
  AvatarStyled,
  ProfileInfo,
  FullName,
  Details,
  Detail,
  ProfileComment
} from "./styled";

const Profile = ({ data, isFetching }) => {
  if (isFetching) return <Loading />;

  const { name, comment, phone, birthDate } = data.patient;

  return (
    <PatientProfile>
      <AvatarStyled>{initialsForAvatar(name)}</AvatarStyled>
      <ProfileInfo>
        <FullName>{name}</FullName>
        <Details>
          <Detail>{[dateToAge(birthDate), phone].join(", ")}</Detail>
        </Details>
      </ProfileInfo>
      <ProfileComment>{comment}</ProfileComment>
    </PatientProfile>
  );
};

export default Profile;
