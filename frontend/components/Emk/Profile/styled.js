import styled from "styled-components";

import { Avatar, LinearProgress } from "@material-ui/core";

const PatientProfile = styled.header`
  position: sticky;
  top: 55px;
  z-index: 1;
  width: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding: 0 24px;
  box-sizing: border-box;
  background: ${p => p.theme.palette.primary.main};
`;

const AvatarStyled = styled(Avatar)`
  height: 56px;
  width: 56px;
  font-size: 24px;
  background: ${({ theme }) => theme.palette.secondary.main};
  color: ${({ theme }) => theme.palette.primary.main};
`;

const ProfileInfo = styled.div`
  padding: 20px;
`;

const FullName = styled.h3`
  display: flex;
  align-items: center;
  margin: 0 0 4px;
  font-size: 1.4rem;
  font-weight: normal;
  color: ${({ theme }) => theme.palette.primary.contrastText};

  svg {
    fill: ${({ theme }) => theme.palette.primary.main};
    margin-left: 5px;
  }
`;

const Details = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
  font-size: 1rem;
  color: ${({ theme }) => theme.palette.secondary.light};

  svg {
    fill: ${({ theme }) => theme.palette.gray.dark};
  }
`;

const Detail = styled.li`
  display: flex;
  align-items: center;
`;

const DetailValue = styled.span`
  padding-left: 8px;
`;

const ProfileComment = styled.div`
  max-width: 800px;
  display: flex;
  justify-content: flex-end;
  align-items: flex-end;
  align-self: flex-end;
  margin-bottom: 20px;
  margin-left: auto;
  color: ${({ theme }) => theme.palette.secondary.light};
  font-size: 0.8rem;
  text-align: right;
`;

const LinearProgressStyled = styled(LinearProgress)`
  flex: 1;
  width: 140px;
  padding: 8px;
  margin: 4px;
  box-sizing: border-box;
  background-color: ${({ theme }) => theme.palette.gray.light};
  & > div {
    background-image: linear-gradient(
      to right,
      ${({ theme }) => theme.palette.gray.light} 0%,
      white 45%,
      white 65%,
      ${({ theme }) => theme.palette.gray.light} 100%
    );
  }
`;

const SettingsIconBlock = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;

const InfoIconBlock = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;

const PopoverBoard = styled.ul`
  padding: 20px;
  font-size: 0.8rem;
  margin: 0;
`;

const BoardItem = styled.li`
  display: flex;
  padding: 4px 0;
  list-style-type: none;
`;

const BoardItemSubList = styled.ul`
  display: flex;
  flex-direction: column;
  margin: 4px 0;
`;

const NameBoardItem = styled.span`
  font-weight: 700;
`;

const SubNameBoardItem = styled.span`
  font-weight: 700;
  margin-left: 6px;
  margin: 8px 0;
  list-style-type: disc;
`;

const InfoBoardItem = styled.span`
  margin-left: 6px;
  font-weight: 500;
`;

const InfoBoardSubItem = styled.li`
  margin: 4px 0 4px 6px;
  font-weight: 500;
`;

export {
  PatientProfile,
  AvatarStyled,
  ProfileInfo,
  FullName,
  Details,
  Detail,
  DetailValue,
  ProfileComment,
  LinearProgressStyled,
  SettingsIconBlock,
  InfoIconBlock,
  PopoverBoard,
  InfoBoardItem,
  NameBoardItem,
  SubNameBoardItem,
  BoardItem,
  BoardItemSubList,
  InfoBoardSubItem
};
