import React from "react";

import Profile from "./Profile";

import { Container, Content } from "./styled";

const Loading = ({ isFetching, children }) => (
  <>
    <Profile isFetching={isFetching} />
    <Container>
      <Content>{children}</Content>
    </Container>
  </>
);

export default Loading;
