import React from "react";
import PropTypes from "prop-types";
import { useRouter } from "next/router";
import { useQuery } from "react-apollo-hooks";

import { EVENTS_BY_PATIENT_ID } from "graphQL/events";
import { PATIENT_BY_ID } from "graphQL/patients";

import Loading from "./Loading";
import { Event } from "components";
import { ControllableList } from "components/common";
import Profile from "./Profile";
import AddEvent from "./AddEvent";
import { Container, Content } from "./styled";

const Emk = ({ children }) => {
  const router = useRouter();

  const { patientId } = router.query;

  const { data, error, loading } = useQuery(PATIENT_BY_ID, {
    variables: {
      id: patientId
    }
  });
  //
  if (loading) return <Loading isFetching={loading}>Загрузка</Loading>;

  return (
    <>
      <Profile isFetching={loading} data={data} />
      <Container>
        <Content>
          <ControllableList
            title="Посещения"
            listPath="events"
            actionTitle="Добавить посещение"
            query={EVENTS_BY_PATIENT_ID}
            variables={{
              sort: "startDate:desc",
              where: { patient: { id: patientId } }
            }}
            searchField="comment"
            Component={props => <Event {...props} />}
            DialogComponent={AddEvent}
            dialogTitle="Добавление посещения"
            dialogComponentProps={{ patientId }}
          />
        </Content>
      </Container>
    </>
  );
};

Emk.propTypes = {
  children: PropTypes.node
};

export default Emk;
