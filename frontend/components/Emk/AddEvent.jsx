import React from "react";
import { Form, Field } from "react-final-form";
import styled from "styled-components";
import { useMutation } from "react-apollo-hooks";
import { Grid, Button } from "@material-ui/core";
import {
  TextFieldAdapter,
  DatePickerAdapter,
  TimePickerAdapter
} from "components/Fields";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import { CREATE_EVENT } from "graphQL/events";
import FnsUtils from "@date-io/date-fns";
import ruLocale from "date-fns/locale/ru";

const AddEvent = ({ query, variables, onClose, patientId }) => {
  const [addEvent, { loading }] = useMutation(CREATE_EVENT);
  const onSubmit = values => {
    addEvent({
      variables: {
        input: {
          data: { ...values, patient: patientId }
        }
      },
      update() {
        onClose();
      },
      refetchQueries: [{ query, variables }]
    });
  };

  return (
    <Wrapper>
      <Form
        initialValues={{
          startDate: new Date()
        }}
        onSubmit={onSubmit}
        render={({ handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <MuiPickersUtilsProvider utils={FnsUtils} locale={ruLocale}>
              <Grid container spacing={3}>
                <Grid item xs={6}>
                  <Field
                    required
                    fullWidth
                    label="Дата посещения"
                    name="startDate"
                    component={DatePickerAdapter}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Field
                    required
                    fullWidth
                    label="Время посещения"
                    name="startDate"
                    component={TimePickerAdapter}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Field
                    fullWidth
                    required
                    name="comment"
                    label="Комментарий"
                    component={TextFieldAdapter}
                  />
                </Grid>
              </Grid>
              <ButtonWrapper>
                <Button
                  disabled={loading}
                  color="primary"
                  variant="contained"
                  type="submit"
                >
                  Добавить
                </Button>
              </ButtonWrapper>
            </MuiPickersUtilsProvider>
          </form>
        )}
      />
    </Wrapper>
  );
};

export default AddEvent;

const Wrapper = styled.div`
  padding: 20px;
  min-width: 600px;
`;

const ButtonWrapper = styled.div`
  margin-top: 20px;
  text-align: right;
`;
