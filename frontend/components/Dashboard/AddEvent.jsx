import React from "react";
import { Form, Field } from "react-final-form";
import styled from "styled-components";
import { useMutation, useQuery } from "react-apollo-hooks";
import { Grid, Button } from "@material-ui/core";
import {
  TextFieldAdapter,
  DatePickerAdapter,
  TimePickerAdapter,
  AutoCompleteAdapter
} from "components/Fields";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import { CREATE_EVENT } from "graphQL/events";
import { PATIENTS } from "graphQL/patients";
import FnsUtils from "@date-io/date-fns";
import ruLocale from "date-fns/locale/ru";

const AddEvent = ({ query, eventDate, onClose, onUpdate }) => {
  const {
    data: patientsData,
    error: errorPatients,
    loading: loadingPatients
  } = useQuery(PATIENTS, {
    // fetchPolicy: "cache-and-network"
  });

  const [addEvent, { loading }] = useMutation(CREATE_EVENT);
  const onSubmit = values => {
    addEvent({
      variables: {
        input: {
          data: {
            comment: values.comment,
            startDate: values.startDate,
            patient: values.patient.id
          }
        }
      },
      update() {
        onUpdate();
        onClose();
      }
    });
  };

  return (
    <Wrapper>
      <Form
        initialValues={{
          startDate: eventDate
        }}
        onSubmit={onSubmit}
        render={({ handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <MuiPickersUtilsProvider utils={FnsUtils} locale={ruLocale}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Field
                    required
                    fullWidth
                    label="Пациент"
                    name="patient"
                    component={AutoCompleteAdapter}
                    data={
                      errorPatients || loadingPatients
                        ? []
                        : patientsData.patients
                    }
                    valuePath="id"
                    viewPath="name"
                  />
                </Grid>
                <Grid item xs={6}>
                  <Field
                    required
                    fullWidth
                    label="Дата посещения"
                    name="startDate"
                    component={DatePickerAdapter}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Field
                    required
                    fullWidth
                    label="Время посещения"
                    name="startDate"
                    component={TimePickerAdapter}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Field
                    fullWidth
                    required
                    name="comment"
                    label="Комментарий"
                    component={TextFieldAdapter}
                  />
                </Grid>
              </Grid>
              <ButtonWrapper>
                <Button
                  disabled={loading}
                  color="primary"
                  variant="contained"
                  type="submit"
                >
                  Добавить
                </Button>
              </ButtonWrapper>
            </MuiPickersUtilsProvider>
          </form>
        )}
      />
    </Wrapper>
  );
};

export default AddEvent;

const Wrapper = styled.div`
  padding: 20px;
`;

const ButtonWrapper = styled.div`
  margin-top: 20px;
  text-align: right;
`;
