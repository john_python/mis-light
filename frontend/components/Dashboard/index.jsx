import React, { useCallback, useState, useEffect } from "react";
import { useQuery } from "react-apollo-hooks";
import { useRouter } from "next/router";
import { formatDateToClient } from "utils/common/formatDate";
import styled from "styled-components";
import { lighten } from "polished";
import Event from "./Event";
import { Grid, IconButton } from "@material-ui/core";
import { EVENTS_BY_STARTDATE } from "graphQL/events";
import { Button } from "@material-ui/core";
import { ChangeViewIcon } from "static/icons";

const from = new Date();
from.setUTCHours(0);
from.setUTCMinutes(0);
from.setUTCSeconds(0);

const to = new Date();
to.setUTCHours(23);
to.setUTCMinutes(59);
to.setUTCSeconds(59);

const Dashboard = () => {
  const router = useRouter();
  const [currentDate, setCurrentDate] = useState(new Date());
  const [fromNowView, setFromNowView] = useState(false);

  const variables = {
    sort: "startDate:asc",
    where: {
      startDate_gte: fromNowView ? currentDate : from,
      startDate_lte: to
    }
  };

  const toggleView = useCallback(() => {
    setFromNowView(!fromNowView);
  }, [fromNowView]);

  const { data, error, loading, refetch } = useQuery(EVENTS_BY_STARTDATE, {
    variables,
    fetchPolicy: "cache-and-network"
  });

  const refetchQuery = useCallback(() => {
    setCurrentDate(new Date());
    refetch(variables);
  }, [variables]);

  useEffect(() => {
    const interval = setInterval(refetchQuery, 60000);

    return () => {
      clearInterval(interval);
    };
  });

  const redirectiToSchedule = useCallback(() => {
    router.push("/schedule");
  });

  const redirectiToPatient = useCallback(id => () => {
    console.log("id");
    router.push(`/patients/${id}`);
  });

  if (loading) return "Загрузка";

  return (
    <Wrapper>
      <Header>
        <HeaderTime>
          <HeaderLeft>
            <h2>{fromNowView ? "Сейчас" : "Сегодня"}</h2>
            <ChangeViewButton onClick={toggleView}>
              <ChangeViewIcon />
            </ChangeViewButton>
          </HeaderLeft>
          <CurrentTime>
            <span>{formatDateToClient(currentDate)}</span>
            <strong>{formatDateToClient(currentDate, "HH:mm")}</strong>
          </CurrentTime>
        </HeaderTime>
      </Header>
      <StyledGrid container spacing={3} alignItems="stretch">
        {!data?.events?.length && (
          <Empty>
            <h1>Список событий на сегодня пуст</h1>
            <p>Перейдите в полное расписание, чтобы создать посещение</p>
            <Button
              color="primary"
              variant="contained"
              onClick={redirectiToSchedule}
            >
              Перейти в полное расписание
            </Button>
          </Empty>
        )}
        {data.events.map(event => (
          <StyledGridItem key={event.id} item md={6} lg={3}>
            <Event {...event} redirectiToPatient={redirectiToPatient} />
          </StyledGridItem>
        ))}
      </StyledGrid>
    </Wrapper>
  );
};
export default Dashboard;

const Wrapper = styled.div``;

const Empty = styled.div`
  margin: auto;
  margin-top: 100px;
  text-align: center;

  p,
  h1 {
    margin: 0;
  }

  button {
    margin-top: 40px;
  }
`;

const StyledGrid = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: -60px 20px 20px;
`;

const StyledGridItem = styled(Grid)`
  display: flex;
`;

const Header = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 12px;
  padding: 24px 24px 120px;
  border-radius: 0 0 20px 20px;
  height: 60px;
  background: ${p => p.theme.palette.primary.main};
  color: ${p => p.theme.palette.primary.contrastText};
`;

const HeaderTime = styled.div`
  margin-top: 30px;
  width: 100%;
  padding: 10px 60px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  color: ${p => p.theme.palette.primary.contrastText};
  background: ${p => lighten(0.07, p.theme.palette.primary.main)};
  border-radius: 30px;

  h2 {
    margin: 0;
    font-size: 1.4rem;
    text-transform: uppercase;
  }
`;

const HeaderLeft = styled.div`
  display: flex;
  align-items: center;
`;

const ChangeViewButton = styled(IconButton)`
  margin-left: 10px;
  width: 42px;
  height: 42px;
  color: ${p => p.theme.palette.primary.contrastText};
`;

const CurrentTime = styled.div`
  span {
    margin-right: 20px;
  }

  strong {
    font-size: 2rem;
  }
`;
