import React from "react";
import styled from "styled-components";
import { Avatar, ListItem } from "@material-ui/core";
import { ManUser, TimeIcon } from "static/icons";
import { formatDateToClient } from "utils/common/formatDate";
import { sliceText } from "utils/common/textFormat";
import { dateToAge } from "utils";
import { compareAsc } from "date-fns";

const Event = ({ id, comment, startDate, patient, redirectiToPatient }) => {
  return (
    <EventWrapper>
      <PersonInfo button onClick={redirectiToPatient(patient.id)}>
        <AvatarStyled>
          <ManUser />
        </AvatarStyled>
        <div>
          <Name>{patient?.name}</Name>
          <Age>{patient?.birthDate && dateToAge(patient.birthDate)}</Age>
        </div>
      </PersonInfo>
      <TimeInfo>
        <Time past={compareAsc(new Date(startDate), new Date())}>
          <TimeIcon />
          {formatDateToClient(startDate, "HH:mm")}
        </Time>
        <Comment>{sliceText(comment, 100)}</Comment>
      </TimeInfo>
    </EventWrapper>
  );
};
export default Event;

const EventWrapper = styled.div`
  margin: 10px;
  box-shadow: ${({ theme }) => theme.options.card.boxShadow};
  border-radius: 4px;
  background: #fff;
  padding-bottom: 10px;
  flex: 1;
`;

const PersonInfo = styled(ListItem)`
  display: flex;
  align-items: center;
  margin: 14px 10px;
  padding: 10px 14px;
  width: unset;
  border-radius: 6px;
`;

const AvatarStyled = styled(Avatar)`
  width: 48px;
  height: 48px;
  margin-right: 24px;
  background: ${({ theme }) => theme.palette.secondary.main};
  color: ${({ theme }) => theme.palette.primary.main};
`;

const Name = styled.div`
  color: ${({ theme }) => theme.palette.text.primary};
  font-weight: 500;
`;

const Age = styled.div`
  color: ${({ theme }) => theme.palette.text.secondary};
`;

const TimeInfo = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 10px 24px 10px;
  flex-direction: column;
`;

const Comment = styled.div`
  display: flex;
  color: ${({ theme }) => theme.palette.text.primary};
  font-size: 1rem;
  text-align: center;
`;

const Time = styled.div`
  display: flex;
  align-items: center;
  font-size: 2rem;
  font-weight: 700;
  margin-bottom: 28px;
  color: ${p =>
    p.past > -1
      ? p.theme.palette.primary.main
      : p.theme.palette.text.secondary};
  
  svg {
    width: 20px;
    height: 20px;
    margin-right: 10px;
  }
`;
