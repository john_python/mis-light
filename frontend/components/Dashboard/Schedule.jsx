import React from "react";
import styled from "styled-components";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction"; // needed for dayClick

// must manually import the stylesheets for each plugin
import "@fullcalendar/core/main.css";
import "@fullcalendar/daygrid/main.css";
import "@fullcalendar/timegrid/main.css";

import ruLocale from "@fullcalendar/core/locales/ru";

import { Dialog, DialogTitle, DialogContent } from "@material-ui/core";
import AddEvent from "./AddEvent";
import EventView from "./EventView";
import ApolloService from "services/ApolloService";
import { EVENTS, UPDATE_EVENT } from "graphQL/events";

const MODE = {
  add: 1,
  edit: 2
};

export default class Schedule extends React.Component {
  calendarComponentRef = React.createRef();

  state = {
    events: [
      // initial event data
      { title: "Event Now", start: new Date() }
    ],

    mode: MODE.add,
    eventDate: null
  };

  componentDidMount() {
    this.getEvents();
  }

  getEvents = async () => {
    const {
      data: { events }
    } = await ApolloService.apollo.query({
      query: EVENTS,
      fetchPolicy: "network-only"
    });

    this.setState({
      events: events.map(({ startDate, comment, id, patient }) => ({
        start: startDate,
        title: comment,
        id,
        extendedProps: { patient }
      }))
    });
  };

  compareDateWithoutMilliSeconds = (d1, d2) =>
    Math.floor(d1.getTime() / 1000) === Math.floor(d2.getTime() / 1000);

  changeEventName = e => this.setState({ eventName: e.target.value });

  addEvent = () => {
    this.setState(
      state => ({
        // add new event data
        events: state.events.concat({
          // creates a new array
          title: state.eventName,
          start: state.eventDate
        })
      }),
      this.closeModal
    );
  };

  changeEventDate = date => this.setState({ eventDate: date });

  eventClick = event => {
    this.setState(
      {
        mode: MODE.edit,
        eventName: event.title,
        eventDate: event.start
      },
      this.openModal
    );
  };

  // modal
  openModal = () => this.setState({ modalOpen: true });
  closeModal = () => this.setState({ modalOpen: false });

  // events
  eventDrop = async ({ oldEvent, event }) => {
    await ApolloService.apollo.mutate({
      mutation: UPDATE_EVENT,
      variables: {
        input: {
          where: {
            id: event.id
          },
          data: {
            startDate: event.start
          }
        }
      }
    });

    this.getEvents();
  };

  handleDateClick = arg => {
    this.openModal();
    this.setState({
      mode: "",
      eventDate: arg.date
    });
  };

  render() {
    const { modalOpen, eventDate, events } = this.state;

    return (
      <Wrapper>
        <FullCalendar
          eventLimit
          locale={ruLocale}
          editable
          defaultView="timeGridWeek"
          header={{
            left: "prev,next today",
            center: "title",
            right: "dayGridMonth,timeGridWeek,timeGridDay,listWeek"
          }}
          plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
          ref={this.calendarComponentRef}
          events={events}
          dateClick={this.handleDateClick}
          // eventClick={this.eventClick}
          eventDrop={this.eventDrop}
          eventRender={EventView}
          eventProps={{ aye: "aaaa" }}
        />
        <Dialog
          open={modalOpen}
          onClose={this.closeModal}
          PaperProps={{ style: { overflowY: "visible" } }}
        >
          <DialogTitle>Добавление события</DialogTitle>
          <StyledDialogContent>
            <AddEvent
              // eventName={eventName}
              eventDate={eventDate}
              // changeEventName={this.changeEventName}
              // addEvent={this.addEvent}
              // editEvent={this.editEvent}
              // changeEventDate={this.changeEventDate}
              onClose={this.closeModal}
              onUpdate={this.getEvents}
            />
          </StyledDialogContent>
        </Dialog>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  padding: 24px;
`;

const StyledDialogContent = styled(DialogContent)`
  overflow-y: visible;
`;
