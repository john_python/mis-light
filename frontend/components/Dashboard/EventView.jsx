import React from "react";
import { render } from "react-dom";
import styled from "styled-components";
import { compareAsc } from "date-fns";
import { formatDateToClient } from "utils/common/formatDate";
import { TimeIcon } from "static/icons";
import { Tooltip } from "@material-ui/core";
import { sliceText } from "utils/common/textFormat";

const EventView = ({ event, el, ...props }) => {
  // Creating `div` to replace the default <a href=""/> for event
  const eventDiv = document.createElement("div");
  // Get classes on the default `a.fc-timeline-event`
  const classes = Array.from(el.classList);
  // Add classes to the new `div`
  eventDiv.classList.add(...classes);

  console.log("EVENT PROPS", props, event);

  render(
    <EventContent past={compareAsc(event.start, new Date())}>
      <Header>
        <TimeIcon />
        <Time>{formatDateToClient(event.start, "HH:mm")}</Time>
        <Name title={event.extendedProps?.patient?.name}>
          {event.extendedProps?.patient?.name}
        </Name>
      </Header>
      <Description title={event.title}>{sliceText(event.title)}</Description>
    </EventContent>,
    eventDiv
  );

  return eventDiv;
};

export default EventView;

const EventContent = styled.div`
  padding: 5px;
  border-radius: 4px;
  background: ${p => (p.past > -1 ? "#016FD1" : "#b9c1ca")};
`;

const Header = styled.div`
  display: flex;
  align-items: center;

  svg {
    width: 16px;
    flex-shrink: 0;
  }
`;

const Time = styled.div`
  margin: 0 5px;
  font-weight: bold;
`;

const Name = styled.div`
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`;

const Description = styled.div`
  margin-top: 5px;
  font-size: 0.8rem;
`;
