import React from "react";
import PropTypes from "prop-types";

import Profile from "./Profile";
import { Header, PageTitle } from "./styled";

const DashboardHeader = ({ title }) => {
  return (
    <Header>
      <PageTitle>{title}</PageTitle>
      <Profile />
    </Header>
  );
};

DashboardHeader.propTypes = {
  title: PropTypes.string.isRequired
};

export default DashboardHeader;
