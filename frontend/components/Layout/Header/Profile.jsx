import React from "react";
import { ProfileItem, UserAvatar } from "./styled";
import { UserIcon } from "static/icons";

const Profile = () => {
  return (
    <ProfileItem button>
      <UserAvatar>
        <UserIcon />
      </UserAvatar>
      Мой профиль
    </ProfileItem>
  );
};

export default Profile;
