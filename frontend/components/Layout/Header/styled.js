import styled from "styled-components";
import { MenuItem, AppBar, ListItem, Avatar } from "@material-ui/core";

const Header = styled(AppBar)`
  width: auto;
  background: ${p => p.theme.palette.primary.main};
  color: ${p => p.theme.palette.primary.contrastText};
  height: 75px;
  box-shadow: none;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  position: sticky;
  top: 0;
`;

const PageTitle = styled.div`
  padding-left: 20px;
  display: flex;
  flex: 1;
  align-items: center;
  margin-left: 0;
  white-space: nowrap;
  font-size: 1.5rem;
  font-weight: bold;
`;

const Option = styled(MenuItem)`
  font-size: 1.4rem;
`;

const Message = styled.div`
  padding: 0 10px;
`;

const ProfileItem = styled(ListItem)`
  height: 100%;
  flex: 0;
  white-space: nowrap;
  
  &:hover {
    background: ${p => p.theme.palette.primary.dark};
  }
`;

const UserAvatar = styled(Avatar)`
  margin-right: 20px;
  background: ${p => p.theme.palette.secondary.main};
  color: ${p => p.theme.palette.secondary.contrastText};
`;

export { Header, PageTitle, Option, Message, ProfileItem, UserAvatar };
