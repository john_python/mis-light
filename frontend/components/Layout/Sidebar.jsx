import React, { useState } from 'react';
import styled from 'styled-components';

import Menu from './Menu';

const SidebarWrapper = styled.div`
  min-width: 80px;
  max-width: 80px;
  width: 80px;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  background: ${({ theme }) => theme.palette.background.secondary};
`;

const SidebarHeader = styled.div`
  display: flex;
  height: 75px;
  width: 100%;
  justify-content: center;
  align-items: center;
  border-bottom: 1px solid ${({ theme }) => theme.palette.divider};
  cursor: pointer;
`;

const Button = styled.div`
  display: flex;
  width: 24px;
  height: 16px;
  display: flex;
  flex-direction: column;
  justify-content: ${({ toggleMenu }) =>
    toggleMenu ? 'center' : 'space-between'};
  align-items: center;

  span {
    display: block;
    width: 24px;
    height: 2px;
    background: ${({ theme }) => theme.palette.link.hover};
    transition: all 0.2s;
    position: ${({ toggleMenu }) => toggleMenu && 'absolute'};
  }

  & > span:nth-child(1) {
    transform: ${({ toggleMenu }) => toggleMenu && 'rotate(45deg)'};
  }

  & > span:nth-child(2) {
    display: ${({ toggleMenu }) => toggleMenu && 'none'};
  }

  & > span:nth-child(3) {
    transform: ${({ toggleMenu }) => toggleMenu && 'rotate(-45deg)'};
  }
`;

const Sidebar = () => {
  const [toggleMenu, setToggleMenu] = useState(false);

  return (
    <SidebarWrapper>
      <SidebarHeader onClick={() => setToggleMenu(!toggleMenu)}>
        <Button toggleMenu={toggleMenu}>
          <span />
          <span />
          <span />
        </Button>
      </SidebarHeader>
      <Menu toggleMenu={toggleMenu} />
    </SidebarWrapper>
  );
};

export default Sidebar;
