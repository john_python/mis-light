import styled from "styled-components";

const Layout = styled.div`
  display: flex;
  height: 100vh;
`;
const Content = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  overflow-y: auto;
`;
const Main = styled.div`
  flex: 1;
  position: relative;
`;

export { Layout, Content, Main };
