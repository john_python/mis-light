import React from "react";
import PropTypes from "prop-types";

import { Layout, Content, Main } from "./styled";

import Sidebar from "./Sidebar";
import Header from "./Header";

const DashboardLayout = ({ title, children }) => {
  return (
    <Layout>
      <Sidebar />
      <Content>
        <Header title={title} />
        <Main>{children}</Main>
      </Content>
    </Layout>
  );
};

DashboardLayout.propTypes = {
  children: PropTypes.node,
  title: PropTypes.node.isRequired
};

export default DashboardLayout;
