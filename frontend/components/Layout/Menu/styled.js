import styled from "styled-components";

import { MenuItem, MenuList } from "@material-ui/core";

const Menu = styled.div`
  position: absolute;
  left: 0;
  top: 75px;
  z-index: 100;
  height: calc(100% - 75px);
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  background: ${({ theme }) => theme.palette.background.secondary};
  box-shadow: ${p => p.theme.options.card.boxShadow};

  span {
    width: ${({ toggleMenu }) => (toggleMenu ? "245px" : "0px")};
  }
`;

const MenuListStyled = styled(MenuList)`
  display: flex;
  flex-direction: column;
`;

const MenuIcon = styled.div`
  width: 42px;
  height: 42px;
  display: flex;
  justify-content: center;
  align-items: center;
  background: ${({ theme }) => theme.palette.gray.light};
  border-radius: 50%;
`;

const MenuText = styled.span`
  height: 60px;
  display: flex;
  align-items: center;
  overflow: hidden;
  font-size: 1rem;
  transition: all 0.2s;
`;

const MenuItemStyled = styled(MenuItem)`
  height: 60px;
  display: flex;
  align-items: center;
  padding: 0 !important;
  transition: all 0.2s;
  cursor: pointer;
  color: ${({ active, theme }) =>
    active ? theme.palette.primary.main : theme.palette.gray.main};
  background: ${({ active, theme }) =>
    active ? theme.palette.primary.main : theme.palette.background.secondary};

  &:hover {
    background: ${({ theme }) => theme.palette.primary.main};
    color: ${({ theme }) => theme.palette.primary.main};
  }

  ${MenuText} {
    color: ${({ active, theme }) =>
      active ? theme.palette.primary.contrastText : theme.palette.gray.main};

    &:hover {
      color: ${({ theme }) => theme.palette.primary.contrastText};
    }
  }
`;

const IconWrapper = styled.div`
  width: 80px;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export {
  Menu,
  MenuListStyled,
  MenuItemStyled,
  IconWrapper,
  MenuIcon,
  MenuText
};
