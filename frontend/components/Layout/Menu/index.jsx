import React from 'react';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import { SIDEBAR_MENU } from 'constants/sidebar';

import {
  Menu,
  MenuListStyled,
  MenuItemStyled,
  IconWrapper,
  MenuIcon,
  MenuText
} from './styled';

const NavMenu = ({ toggleMenu }) => {
  const router = useRouter();
  const isActive = arg => {
    if (!arg) return false;
    // проверка для главной страницы
    if (arg === '/') {
      return arg === router.route;
    } else {
      return router.route.includes(arg);
    }
  };
  const redirectTo = path => () => {
    path && router.push(path);
  };

  return (
    <Menu toggleMenu={toggleMenu}>
      <MenuListStyled>
        {SIDEBAR_MENU.map(({ title, key, path, icon }) => (
          <MenuItemStyled
            key={key}
            active={isActive(path)}
            onClick={redirectTo(path)}
          >
            <IconWrapper>
              <MenuIcon>{icon}</MenuIcon>
            </IconWrapper>
            <MenuText>{title}</MenuText>
          </MenuItemStyled>
        ))}
      </MenuListStyled>
    </Menu>
  );
};

NavMenu.propTypes = {
  toggleMenu: PropTypes.bool
};

export default NavMenu;
