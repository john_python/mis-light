import React from "react";
import Layout from "components/Layout";
import Dashboard from "components/Dashboard";

const Home = () => (
  <Layout title="Главная">
    <Dashboard />
  </Layout>
);

export default Home;
