import React from "react";
import dynamic from "next/dynamic";
import Layout from "components/Layout";

// FullCalendar не работает на SSR
const Schedule = dynamic(() => import("components/Dashboard/Schedule"), {
  ssr: false,
  loading: () => "Загрузка"
});

const SchedulePage = () => (
  <Layout title="Главная">
    <Schedule />
  </Layout>
);

export default SchedulePage;
