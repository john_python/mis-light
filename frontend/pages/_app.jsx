import React from "react";
import App from "next/app";
import Head from "next/head";
import ThemeProvider from "core/theme/ThemeProvider";
import getPageContext from "core/utils/getPageContext";
import withApollo from "core/utils/withApollo";
import GlobalStyle from "core/theme/globalStyles";
import { ApolloProvider } from "react-apollo";
import { ApolloProvider as ApolloHooksProvider } from "react-apollo-hooks";
import ApolloService from "services/ApolloService";

@withApollo
class MyApp extends App {
  constructor(props, ctx) {
    super(props, ctx);
    this.pageContext = getPageContext();

    if(process.browser) {
      ApolloService.init(props.apolloClient);
    }
  }

  componentDidMount() {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentNode.removeChild(jssStyles);
    }
  }

  render() {
    const { Component, pageProps, apolloClient } = this.props;

    return (
      <ApolloProvider client={apolloClient}>
        <ApolloHooksProvider client={apolloClient}>
          <ThemeProvider pageContext={this.pageContext}>
            <>
              <Head>
                <title>Астрал МИС</title>
              </Head>
              <GlobalStyle />
              <Component {...pageProps} pageContext={this.pageContext} />
            </>
          </ThemeProvider>
        </ApolloHooksProvider>
      </ApolloProvider>
    );
  }
}

export default MyApp;
