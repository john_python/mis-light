import React from "react";
import { Layout, Emk } from "components";

class Visits extends React.Component {
  static async getInitialProps({ query }) {
    const { patientId } = query;
    return { patientId };
  }

  render() {
    const { patientId } = this.props;
    return (
      <Layout title="Карта пациента">
        <Emk patientId={patientId} />
      </Layout>
    );
  }
}

export default Visits;
