import React from "react";

import Layout from "components/Layout";
import { PatientRegistry } from "components";

const Patients = () => (
  <Layout title="Пациенты">
    <PatientRegistry />
  </Layout>
);

export default Patients;
