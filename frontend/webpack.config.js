const path = require('path');

module.exports = {
  config: {
    resolve: {
      alias: {
        components: path.resolve(__dirname, './components'),
        constants: path.resolve(__dirname, './constants'),
        core: path.resolve(__dirname, './core'),
        utils: path.resolve(__dirname, './utils'),
        theme: path.resolve(__dirname, './core/theme'),
        services: path.resolve(__dirname, './services'),
        sources: path.resolve(__dirname, './sources'),
        graphQL: path.resolve(__dirname, './graphql'),
        static: path.resolve(__dirname, './static')
      }
    }
  }
};
