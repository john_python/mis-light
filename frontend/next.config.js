const merge = require("webpack-merge");
const { config: cfg } = require("./webpack.config");
const withTM = require("@weco/next-plugin-transpile-modules");

const {
  GRAPHQL_EXTERNAL_URL = "https://backend-light.amis.astral-dev.net",
  GRAPHQL_URL = GRAPHQL_EXTERNAL_URL
} = process.env;

module.exports = withTM({
  webpack(config, { isServer }) {
    const { rules } = config.module;
    rules.push({
      test: /\.svg$/,
      use: [
        {
          loader: "@svgr/webpack",
          options: {
            svgProps: {
              fill: "currentColor"
            }
          }
        }
      ]
    });

    rules.push({
      test: /\.css$/i,
      use: ["style-loader", "css-loader"]
    });

    return merge(config, cfg);
  },
  onDemandEntries: {
    maxInactiveAge: 120 * 1e3,
    pagesBufferLength: 3
  },
  transpileModules: ["lodash-es"],
  publicRuntimeConfig: {
    graphQL: GRAPHQL_URL,
    graphQLExternal: GRAPHQL_EXTERNAL_URL
  },
  env: {
    SC_DISABLE_SPEEDY: true
  }
});
