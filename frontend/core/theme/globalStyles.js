import { createGlobalStyle } from "styled-components";
// import fonts from './fonts';

const GlobalStyle = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Roboto:400,500,700&display=swap');


  html {
    font-size: 16px;
  }

  body {
    font-family: "Roboto", sans-serif;
    font-size: 1rem;
  }

  h1,h2 {
    font-weight: 600;
    letter-spacing: -0.2px;
  }
  
  h3 {
    font-weight: 500;
    letter-spacing: -0.2px;
  }
  
  h4,h5,h6 {
    font-weight: normal;
  }
  
  h1 {
    font-size: 2.4rem;
  }
  
  h2 {
    font-size: 2rem;
  }
  
  h3 {
    font-size: 1.6rem;
  }
  
  h4 {
    font-size: 1.4rem;
  }
  
  h5 {
    font-size: 1.2rem;
  }
  
  h6 {
    font-size: 1rem;
  }
  
  ::-webkit-scrollbar {
    width: 5px;
    height: 5px;
    background: transparent;
  }
  
  ::-webkit-scrollbar-track {
    border-radius: 3px;
    background: rgba(0, 0, 0, 0.1);
  }
  
  ::-webkit-scrollbar-thumb {
    background: #aaa;
    border-radius: 2px;
  }
  
  #nprogress {
    pointer-events: none;
  }
  
  #nprogress .bar {
    background: #0066FF;
  
    position: fixed;
    z-index: 1031;
    top: 0;
    left: 0;
  
    width: 100%;
    height: 3px;
  }
  
  /* Fancy blur effect */
  #nprogress .peg {
    display: block;
    position: absolute;
    right: 0px;
    width: 100px;
    height: 100%;
    box-shadow: 0 0 10px #fff, 0 0 5px #fff;
    opacity: 1;
  
    -webkit-transform: rotate(3deg) translate(0px, -4px);
    -ms-transform: rotate(3deg) translate(0px, -4px);
    transform: rotate(3deg) translate(0px, -4px);
  }
  
  /* Remove these to get rid of the spinner */
  #nprogress .spinner {
    display: none;
  }
  
  .nprogress-custom-parent {
    overflow: hidden;
    position: relative;
  }
  
  .nprogress-custom-parent #nprogress .spinner,
  .nprogress-custom-parent #nprogress .bar {
    position: absolute;
  }
  
  @-webkit-keyframes nprogress-spinner {
    0% {
      -webkit-transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
    }
  }
  @keyframes nprogress-spinner {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
  
  .fc-time-grid-event {
    overflow: hidden;  
  }
  .fc-event {
    border-color: #F2F5FA !important;
    background: transparent !important;
  }
`;

export default GlobalStyle;
