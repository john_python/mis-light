import React from "react";
import { ThemeProvider } from "styled-components";
import { CssBaseline } from "@material-ui/core";
import getPageContext from "core/utils/getPageContext";
import {
  StylesProvider,
  ThemeProvider as MUIThemeProvider
} from "@material-ui/styles";

class MyThemeProvider extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.pageContext = this.props.pageContext || getPageContext();
  }

  pageContext = null;

  render() {
    const { muiTheme, theme } = this.pageContext;
    const { children } = this.props;

    return (
      <StylesProvider injectFirst>
        <MUIThemeProvider theme={muiTheme}>
          <CssBaseline />
          <ThemeProvider theme={theme}>{children}</ThemeProvider>
        </MUIThemeProvider>
      </StylesProvider>
    );
  }
}

export default MyThemeProvider;
