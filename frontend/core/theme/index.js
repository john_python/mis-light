import { darken, lighten, rgba } from "polished";

const defaultPalette = {
  primary: "#016FD1",
  secondary: "#E5F0FF",
  tertiary: "#191D3A",
  body: "#F2F5FA",
  card: "#FFFFFF",
  gray: "#94A4b7"
};

const statuses = {
  error: "#FF5252",
  warning: "#FF9152",
  success: "#0CCA13"
};

/**
 * Генерация темы
 * @param palette - набор основных цветов
 * @returns {muiTheme, theme}
 */
export const createTheme = (
  { primary, secondary, tertiary, body, card, gray } = defaultPalette,
  { error, warning, success } = statuses
) => {
  const theme = {
    palette: {
      gray: {
        light: lighten(0.32, gray),
        main: gray,
        dark: darken(0.135, gray)
      },

      // Основные цвета элементов
      primary: {
        light: lighten(0.15, primary),
        main: primary,
        dark: darken(0.12, primary),
        contrastText: "#fff"
      },
      secondary: {
        light: darken(0.06, secondary),
        main: secondary,
        dark: darken(0.12, secondary),
        contrastText: primary
      },
      default: {
        light: secondary,
        main: card,
        dark: darken(0.1, secondary),
        contrastText: primary
      },
      disabled: {
        main: rgba(secondary, 0.2),
        contrastText: rgba(secondary, 0.15)
      },
      // Статусы
      error: {
        light: lighten(0.1, error),
        main: error,
        dark: darken(0.1, error),
        contrastText: "#fff"
      },
      warning: {
        light: lighten(0.1, warning),
        main: warning,
        dark: darken(0.1, warning),
        contrastText: "#fff"
      },
      success: {
        light: lighten(0.1, success),
        main: success,
        dark: darken(0.1, success),
        contrastText: "#fff"
      },
      // Текст
      text: {
        primary: tertiary,
        secondary: gray
      },
      // разделитель
      divider: rgba(gray, 0.15),
      // Ссылки
      link: {
        main: darken(0.2, primary),
        hover: darken(0.3, primary),
        active: primary
      },
      // Фон
      background: {
        primary: body,
        secondary: card
      }
    },
    options: {
      card: {
        boxShadow: "0px 2px 8px rgba(0, 0, 0, 0.14)",
        boxShadowHover: "0px 3px 10px rgba(0, 0, 0, 0.25)"
      }
    }
  };

  const { palette } = theme;

  // Переопределение темы Material UI
  const overrides = {
    palette: {
      primary: palette.primary,
      secondary: palette.secondary,
      error: palette.error,
      text: palette.text,
      divider: palette.divider,
      background: {
        paper: palette.background.secondary,
        default: palette.background.primary
      }
    },
    typography: {
      fontSize: 14
    },
    overrides: {
      MuiTooltip: {
        tooltip: {
          fontSize: 14
        }
      }
    }
  };

  return { theme, overrides };
};
