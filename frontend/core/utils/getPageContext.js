import { createTheme } from 'core/theme';
import { createMuiTheme } from '@material-ui/core';

function createPageContext() {
  // Создание темы
  const { overrides, theme } = createTheme();
  const muiTheme = createMuiTheme(overrides);

  return {
    theme,
    muiTheme
  };
}

export default function getPageContext() {
  // Make sure to create a new context for every server-side request so that data
  // isn't shared between connections (which would be bad).
  if (!process.browser) {
    return createPageContext();
  }

  // Reuse context on the client-side.
  if (!global.__INIT_MATERIAL_UI__) {
    global.__INIT_MATERIAL_UI__ = createPageContext();
  }

  return global.__INIT_MATERIAL_UI__;
}
