import { ApolloClient, InMemoryCache, HttpLink } from 'apollo-boost';
import { buildAxiosFetch } from 'axios-fetch';
import axios from 'axios';
import getConfig from 'next/config';

let apolloClient = null;

function create(initialState) {
  const {
    publicRuntimeConfig: { graphQL, graphQLExternal }
  } = getConfig();

  return new ApolloClient({
    connectToDevTools: process.browser,
    ssrMode: !process.browser,
    ssrForceFetchDelay: 100,
    link: new HttpLink({
      uri: `${process.browser ? graphQLExternal : graphQL}/graphql`,
      fetch: buildAxiosFetch(axios)
    }),
    cache: new InMemoryCache().restore(initialState || {})
  });
}

export default function initApollo(initialState) {
  if (!process.browser) {
    return create(initialState);
  }

  if (!apolloClient) {
    apolloClient = create(initialState);
  }

  return apolloClient;
}
