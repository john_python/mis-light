import React from "react";
import initApollo from "./initApollo";
import Head from "next/head";
import { getDataFromTree } from "react-apollo";
import { renderToString } from "react-dom/server";
import { getMarkupFromTree } from "react-apollo-hooks";
import ApolloService from "services/ApolloService";

export default App => {
  return class Apollo extends React.Component {
    static async getInitialProps(ctx) {
      const {
        Component,
        router,
        ctx: { req, res }
      } = ctx;

      const apollo = initApollo();
      ApolloService.init(apollo);

      let appProps = {};
      if (App.getInitialProps) {
        appProps = await App.getInitialProps(ctx);
      }

      if (!process.browser) {
        try {
          // Run all GraphQL queries
          await getDataFromTree(
            <App
              {...appProps}
              Component={Component}
              router={router}
              apolloClient={apollo}
            />
          );
          // Create static markup for SSR HTML export
          await getMarkupFromTree({
            renderFunction: renderToString,
            tree: (
              <App
                {...appProps}
                Component={Component}
                router={router}
                apolloClient={apollo}
              />
            )
          });
        } catch (error) {
          // console.error('Error while running `getDataFromTree`', error);
        }
        Head.rewind();
      }

      const apolloState = {};
      // Extract query data from the Apollo store
      apolloState.data = apollo.cache.extract();
      return {
        ...appProps,
        apolloState
      };
    }

    constructor(props) {
      super(props);
      const { apolloState } = props;
      this.apolloClient = initApollo(apolloState.data);
    }

    render() {
      return <App {...this.props} apolloClient={this.apolloClient} />;
    }
  };
};
