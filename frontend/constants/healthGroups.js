const HEALTH_GROUPS = {
  First: 'I',
  Second: 'II',
  ThirdA: 'III-a',
  ThridB: 'III-b'
};

export default HEALTH_GROUPS;
