import { HomeIcon, ScheduleIcon, UserIcon } from "static/icons";

export const SIDEBAR_MENU = [
  {
    title: "Главная",
    path: "/",
    key: "mainPage",
    icon: <HomeIcon />
  },
  {
    title: "Расписание",
    path: "/schedule",
    key: "schedule",
    icon: <ScheduleIcon />
  },
  {
    title: "Пациенты",
    path: "/patients",
    key: "patients",
    icon: <UserIcon />
  }
];
