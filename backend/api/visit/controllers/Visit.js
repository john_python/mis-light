'use strict';

/**
 * Visit.js controller
 *
 * @description: A set of functions called "actions" for managing `Visit`.
 */

module.exports = {

  /**
   * Retrieve visit records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.visit.search(ctx.query);
    } else {
      return strapi.services.visit.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a visit record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.visit.fetch(ctx.params);
  },

  /**
   * Count visit records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.visit.count(ctx.query, populate);
  },

  /**
   * Create a/an visit record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.visit.add(ctx.request.body);
  },

  /**
   * Update a/an visit record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.visit.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an visit record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.visit.remove(ctx.params);
  }
};
